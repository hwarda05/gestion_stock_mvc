package com.warda.gestionstock.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class MvtStock implements Serializable {

	@Id
	@GeneratedValue
	private long idMvtStock;
	
	@ManyToOne
	 @JoinColumn(name="idArticle")
	private Article article;

	public long getIdMvtStock() {
		return idMvtStock;
	}

	public void setIdMvtStock(long idMvtStock) {
		this.idMvtStock = idMvtStock;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
	
}

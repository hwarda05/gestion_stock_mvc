package com.warda.gestionstock.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Article implements Serializable{
	
	@Id
	@GeneratedValue
	private long idArticle;
	
	private String codeArticle;
	
	private String designiation;
	
	//big decimal est une classe qui contient des m�thode qui va nous permetre de faire des calcules et de ns facilit� les taches
	private BigDecimal prixUnitaireHt;
	
    private BigDecimal tauxTva;
    
    private BigDecimal prixUnitaireTtc;
   
    private String photos;

    
    //chaque article poss�de une categorie  
    @ManyToOne
    @JoinColumn(name="idCategorie")
    private Categorie categorie;
    
    @OneToMany(mappedBy="article")
    List<LigneCommandeClient> ligneCommandeClients;
    
    @OneToMany(mappedBy="article")
    List<LigneCommandeFournisseur> ligneCommandeFournisseurs;
    
    @OneToMany(mappedBy="article")
    List<LigneVente> ligneVentes;
    
    @OneToMany(mappedBy="article")
    List<MvtStock> mvtStocks;
    

	public Article() {
	}

	public long getIdArticle() {
		return idArticle;
	}

	public void setIdArticle(long idArticle) {
		this.idArticle = idArticle;
	}

	public String getCodeArticle() {
		return codeArticle;
	}

	public void setCodeArticle(String codeArticle) {
		this.codeArticle = codeArticle;
	}

	public String getDesigniation() {
		return designiation;
	}

	public void setDesigniation(String designiation) {
		this.designiation = designiation;
	}

	public BigDecimal getPrixUnitaireHt() {
		return prixUnitaireHt;
	}

	public void setPrixUnitaireHt(BigDecimal prixUnitaireHt) {
		this.prixUnitaireHt = prixUnitaireHt;
	}

	public BigDecimal getTauxTva() {
		return tauxTva;
	}

	public void setTauxTva(BigDecimal tauxTva) {
		this.tauxTva = tauxTva;
	}

	public BigDecimal getPrixUnitaireTtc() {
		return prixUnitaireTtc;
	}

	public void setPrixUnitaireTtc(BigDecimal prixUnitaireTtc) {
		this.prixUnitaireTtc = prixUnitaireTtc;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public String getPhotos() {
		return photos;
	}

	public void setPhotos(String photos) {
		this.photos = photos;
	}

	public List<LigneCommandeClient> getLigneCommandeClients() {
		return ligneCommandeClients;
	}

	public void setLigneCommandeClients(List<LigneCommandeClient> ligneCommandeClients) {
		this.ligneCommandeClients = ligneCommandeClients;
	}

	public List<LigneCommandeFournisseur> getLigneCommandeFournisseurs() {
		return ligneCommandeFournisseurs;
	}

	public void setLigneCommandeFournisseurs(List<LigneCommandeFournisseur> ligneCommandeFournisseurs) {
		this.ligneCommandeFournisseurs = ligneCommandeFournisseurs;
	}

	public List<LigneVente> getLigneVentes() {
		return ligneVentes;
	}

	public void setLigneVentes(List<LigneVente> ligneVentes) {
		this.ligneVentes = ligneVentes;
	}

	public List<MvtStock> getMvtStocks() {
		return mvtStocks;
	}

	public void setMvtStocks(List<MvtStock> mvtStocks) {
		this.mvtStocks = mvtStocks;
	}
    
}

package com.warda.gestionstock.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Vente implements Serializable {

	@Id
	@GeneratedValue
	private long idVente;
	
	
	@OneToMany(mappedBy="vente")
	List<LigneVente> ligneVentes;


	public long getIdVente() {
		return idVente;
	}


	public void setIdVente(long idVente) {
		this.idVente = idVente;
	}


	public List<LigneVente> getLigneVentes() {
		return ligneVentes;
	}


	public void setLigneVentes(List<LigneVente> ligneVentes) {
		this.ligneVentes = ligneVentes;
	}
	
}
